//
//  MasterViewController.swift
//  TestMobile
//
//  Created by Alejandro Garcia on 4/2/18.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import UIKit

protocol PostSelectionDelegate: class {
    func postSelected(_ newPost: Post)
}

class MasterViewController: UITableViewController {
    
    weak var delegate: PostSelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    var posts = [
        Post(
            id: 1,
            userid: 3,
            title: "titulo 1",
            description: "Lorem ipsum 1 sit amet, wisi tortor vel class quisque mattis augue, ex in et sit suspendisse eu. Id in duis etiam felis ut odio, lorem dolore, vestibulum vel, lectus arcu sed dignissim, sed eros urna. Ullamcorper elit nullam lectus nullam dolor, velit magna auctor integer consectetuer aliquam, nec aliquet. Nam libero, non rutrum. Molestie diam sociis vel, volutpat interdum nec, in massa volutpat erat faucibus, odio pellentesque eros fusce. Libero diam et at, lacus odio error, aliquet elementum nunc ac primis, iaculis dolor ipsum cras, diam in. Nec feugiat consequat, accusantium egestas aliquam quam neque, facilisis ut sem ipsum, vulputate aliquet laoreet nunc ac. Gravida justo urna vitae quis. Enim fringilla quam imperdiet mollis ac magna. Ante tincidunt, amet adipiscing amet id, id donec risus sit, mi lacus provident lorem. "
        ),
        Post(
            id: 2,
            userid: 54,
            title: "titulo 2",
            description: "Lorem ipsum 2 sit amet, wisi tortor vel class quisque mattis augue, ex in et sit suspendisse eu. Id in duis etiam felis ut odio, lorem dolore, vestibulum vel, lectus arcu sed dignissim, sed eros urna. Ullamcorper elit nullam lectus nullam dolor, velit magna auctor integer consectetuer aliquam, nec aliquet. Nam libero, non rutrum. Molestie diam sociis vel, volutpat interdum nec, in massa volutpat erat faucibus, odio pellentesque eros fusce. Libero diam et at, lacus odio error, aliquet elementum nunc ac primis, iaculis dolor ipsum cras, diam in. Nec feugiat consequat, accusantium egestas aliquam quam neque, facilisis ut sem ipsum, vulputate aliquet laoreet nunc ac. Gravida justo urna vitae quis. Enim fringilla quam imperdiet mollis ac magna. Ante tincidunt, amet adipiscing amet id, id donec risus sit, mi lacus provident lorem."
        ),
        Post(
            id: 3,
            userid: 76,
            title: "titulo 3",
            description: "Lorem ipsum 3 sit amet, wisi tortor vel class quisque mattis augue, ex in et sit suspendisse eu. Id in duis etiam felis ut odio, lorem dolore, vestibulum vel, lectus arcu sed dignissim, sed eros urna. Ullamcorper elit nullam lectus nullam dolor, velit magna auctor integer consectetuer aliquam, nec aliquet. Nam libero, non rutrum. Molestie diam sociis vel, volutpat interdum nec, in massa volutpat erat faucibus, odio pellentesque eros fusce. Libero diam et at, lacus odio error, aliquet elementum nunc ac primis, iaculis dolor ipsum cras, diam in. Nec feugiat consequat, accusantium egestas aliquam quam neque, facilisis ut sem ipsum, vulputate aliquet laoreet nunc ac. Gravida justo urna vitae quis. Enim fringilla quam imperdiet mollis ac magna. Ante tincidunt, amet adipiscing amet id, id donec risus sit, mi lacus provident lorem."
        ),
        Post(
            id: 4,
            userid: 78,
            title: "titulo 4",
            description: "Lorem ipsum 4 sit amet, wisi tortor vel class quisque mattis augue, ex in et sit suspendisse eu. Id in duis etiam felis ut odio, lorem dolore, vestibulum vel, lectus arcu sed dignissim, sed eros urna. Ullamcorper elit nullam lectus nullam dolor, velit magna auctor integer consectetuer aliquam, nec aliquet. Nam libero, non rutrum. Molestie diam sociis vel, volutpat interdum nec, in massa volutpat erat faucibus, odio pellentesque eros fusce. Libero diam et at, lacus odio error, aliquet elementum nunc ac primis, iaculis dolor ipsum cras, diam in. Nec feugiat consequat, accusantium egestas aliquam quam neque, facilisis ut sem ipsum, vulputate aliquet laoreet nunc ac. Gravida justo urna vitae quis. Enim fringilla quam imperdiet mollis ac magna. Ante tincidunt, amet adipiscing amet id, id donec risus sit, mi lacus provident lorem."
        ),
        Post(
            id: 5,
            userid: 23,
            title: "titulo 5",
            description: "Lorem ipsum 5 sit amet, wisi tortor vel class quisque mattis augue, ex in et sit suspendisse eu. Id in duis etiam felis ut odio, lorem dolore, vestibulum vel, lectus arcu sed dignissim, sed eros urna. Ullamcorper elit nullam lectus nullam dolor, velit magna auctor integer consectetuer aliquam, nec aliquet. Nam libero, non rutrum. Molestie diam sociis vel, volutpat interdum nec, in massa volutpat erat faucibus, odio pellentesque eros fusce. Libero diam et at, lacus odio error, aliquet elementum nunc ac primis, iaculis dolor ipsum cras, diam in. Nec feugiat consequat, accusantium egestas aliquam quam neque, facilisis ut sem ipsum, vulputate aliquet laoreet nunc ac. Gravida justo urna vitae quis. Enim fringilla quam imperdiet mollis ac magna. Ante tincidunt, amet adipiscing amet id, id donec risus sit, mi lacus provident lorem."
        ),
    ]

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return posts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
        let post = posts[indexPath.row]
        cell.title.text = post.title
        cell.read.alpha = 0
        cell.favorite.alpha = 0
        if(indexPath.row <= 20) {
            cell.read.alpha = 1
        }
        if(post.read) {
            cell.read.image = UIImage(named: "read")
        }
        if(post.favourite) {
            cell.favorite.alpha = 1
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedPost = posts[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath) as! TableViewCell
        
        selectedPost.read = true
        posts[indexPath.row] = selectedPost
        cell.read.image = UIImage(named: "read")
        
        
        delegate?.postSelected(selectedPost)
        
        if let detailViewController = delegate as? DetailViewController,
            let detailNavigationController = detailViewController.navigationController {
            splitViewController?.showDetailViewController(detailNavigationController, sender: nil)
        }
        
        
    }

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            posts.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
