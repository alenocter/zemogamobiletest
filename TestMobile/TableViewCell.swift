//
//  TableViewCell.swift
//  TestMobile
//
//  Created by Alejandro Garcia on 5/2/18.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var favorite: UIImageView!
    @IBOutlet weak var read: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
