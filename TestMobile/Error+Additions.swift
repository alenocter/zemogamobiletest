//
//  Error+Additions.swift
//  TestMobile
//
//  Created by Alejandro Garcia on 5/2/18.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import UIKit

extension NSError {
    
    class func error(domain:String, code:Int, description:String ) -> Error {
        return NSError(domain: domain,
                       code: code,
                       userInfo: [ NSLocalizedDescriptionKey : description, NSDebugDescriptionErrorKey : description])
    }
    
    class func show(_ error:Error, onViewController:UIViewController)  {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alert.title = "Error"
        alert.message = error.localizedDescription
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        onViewController.present(alert, animated: true, completion: nil)
    }
}
