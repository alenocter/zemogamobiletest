//
//  Post.swift
//  TestMobile
//
//  Created by Alejandro Garcia on 4/2/18.
//  Copyright © 2018 Zemoga. All rights reserved.
//

///Protocol for objects retrieved by the server.


import UIKit

class Post {
    
    var id: Int
    var userid: Int
    var title: String
    var description: String
    var favourite: Bool
    var read: Bool
   
    init(id: Int, userid: Int, title: String, description: String, favourite: Bool = true, read: Bool = false ) {
        self.id = id
        self.userid = userid
        self.title = title
        self.description = description
        self.favourite = favourite
        self.read = read
    }
    
}
