//
//  APIClient.swift
//  TestMobile
//
//  Created by Alejandro Garcia on 5/2/18.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import UIKit

let APIClientErrorDomain = "APIClientErrorDomain"
let APIClientErrorUnsuccessfulRequest   = 500
let APIClientErrorParseError            = 501

class APIClient: NSObject {
    
    
    /// Singleton instance
    static let sharedInstance = APIClient()
    
    
    /// - parameter callback        Callback called once the server responds.
    func executeRequest(parameters: [String: Any] = [:], callback:@escaping(_ result: [[String: Any]]?,_ error: Error?) -> Void){
        
        var urlRequest = URLRequest(url: URL(string: "https://jsonplaceholder.typicode.com/posts")!)
        
        urlRequest.httpMethod = "GET"
        urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        
        //        if let allHeaders = headers {
        //            for (key, value) in allHeaders {
        //                 urlRequest.addValue(value, forHTTPHeaderField: key)
        //            }
        //        }
        
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, urlResponse, error) in
            
            if let requestError = error {
                callback(nil, requestError)
            } else {
                
                let response = urlResponse as! HTTPURLResponse
                
                if response.statusCode == 200 {
                    if let jsonData = data {
                        do {
                            let json = try JSONSerialization.jsonObject(with: jsonData) as? [[String: Any]]
                            callback(json, nil)
                        } catch {
                            do {
                                let json = try JSONSerialization.jsonObject(with: jsonData) as? [String: Any]
                                callback([json!], nil)
                            } catch {
                                callback(nil, error)
                            }
                        }
                    }
                } else {
                    
                    let error = NSError.error(domain: APIClientErrorDomain,
                                              code: APIClientErrorUnsuccessfulRequest,
                                              description: "Unsuccessful Request status code: \(response.statusCode)")
                    callback(nil, error)
                }
            }
        }
        
        dataTask.resume()
    }
    
    /// Creates a query string from a dictionary
    ///
    /// - parameter fromParameters: The dictionary.
    //    static func queryString(fromParameters:[String:Any]) -> String {
    //        guard fromParameters.count > 0 else { return "" }
    //
    //        var parameterString = ""
    //        for (key, value) in fromParameters {
    //            if parameterString.count > 0  {
    //                parameterString.append("&")
    //            }
    //            parameterString.append(key + "=" + (value is Int ? String(describing: value) : value as! String))
    //        }
    //        return "?" + parameterString
    //    }
}

// MARK: - Messages API extension

extension APIClient {
    
    /// Retrieves the top messages
    ///
    /// - parameter callback:     The service path.
    func getAllPosts(callback: @escaping ([Post]?, Error?) -> Void){
        //        let queryString = APIClient.queryString(fromParameters: [:])
        //
        //        let headersDic = [
        //            "Accept": "application/json",
        //            "Content-Type" :"application/json"
        //        ]
        //
        APIClient.sharedInstance.executeRequest(parameters: [:]) { (jsonResponse, error) in
            if (error != nil){
                callback(nil,error)
            } else {
                
                let unableToParseError = NSError.error(domain: APIClientErrorDomain,
                                                       code: APIClientErrorParseError,
                                                       description: "Unable to parse posts.")
                if let json = jsonResponse {
                    guard let postsJson = json as [[String:Any]]? else {
                        return callback(nil,unableToParseError)
                    }
                    
                    var posts = [Post]()
                    for postJson in postsJson{
                        if let post = Post(json: postJson){
                            posts.append(post)
                        }
                    }
                    
                    callback(posts,nil)
                } else {
                    callback(nil,unableToParseError)
                }
            }
        }
    }
}

